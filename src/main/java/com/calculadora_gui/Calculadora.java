package com.calculadora_gui;

import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;



public class Calculadora extends JFrame {
    //ATRIBUTOS
    private JLabel lblNum_1;
    private JLabel lblNum_2;
    private JLabel lblResultado;
    private JTextField campo_1;
    private JTextField campo_2;
    private JButton btnSumar;
    private JButton btnElevar;

    //CONSTRUCTOR
    public Calculadora(){
        //Poner titulo a la ventana
        this.setTitle("Calculadora GUI");
        //Configurar tamaño de la ventana y coordenadas
        this.setBounds(50, 50, 260, 230);
        //Configurar botón de cerrar
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        //Eliminar el esquema que trae por defecto el JFrame (trae por defecto un FlowLayout)
        this.getContentPane().setLayout(null);

        //Inicializar atributos

        lblNum_1 = new JLabel("Número 1: ");
        lblNum_1.setBounds(20, 20, 100, 30);
        //Agregar la etiqueta al Frame
        this.add(lblNum_1);

        campo_1 = new JTextField();
        campo_1.setBounds(120, 20, 100, 30);
        this.add(campo_1);

        lblNum_2 = new JLabel("Número 2: ");
        lblNum_2.setBounds(20, 60, 100, 30);
        this.add(lblNum_2);

        campo_2 = new JTextField();
        campo_2.setBounds(120, 60, 100, 30);
        this.add(campo_2);

        btnSumar = new JButton("Sumar");
        btnSumar.setBounds(30, 100, 100, 30);
        this.add(btnSumar);

        lblResultado = new JLabel("Resultado: ");
        lblResultado.setBounds(75, 140, 100, 30);
        this.add(lblResultado);

        btnElevar = new JButton("n1^n2");
        btnElevar.setBounds(130, 100, 100, 30);
        this.add(btnElevar);

        //MANEJADOR DE EVENTOS
        //Añadir manejador de eventos a btnSumar
        btnSumar.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //Capturar los datos de los campos de texto
                int num_1 = Integer.parseInt(campo_1.getText());
                int num_2 = Integer.parseInt( campo_2.getText() );
                int suma = sumar(num_1, num_2);
                //Sobreescribir el texto del label 'Resultado'
                lblResultado.setText("Resultado: "+suma);
                limpiarCampos();
            }
        } );


        btnElevar.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //Capturar los datos de los campos de texto
                int num_1 = Integer.parseInt(campo_1.getText());
                int num_2 = Integer.parseInt( campo_2.getText() );
                double resultado = Math.pow(num_1, num_2);
                //Setear el label 'Resultado'
                lblResultado.setText("Resultado: "+resultado);
                limpiarCampos();
            }
        } );

        //Mostrar ventana
        this.setVisible(true);
    }

    public int sumar(int n1, int n2){
        return (n1+n2);
    }

    public void limpiarCampos(){
        campo_1.setText("");
        campo_2.setText("");
    }

}
